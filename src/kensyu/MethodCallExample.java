package kensyu;

class Point{
	int x ;
	int y ;
	void printPotion () {
		System.out.println("座標は（" + this.x + "," + this.y + ")です。") ;
	}
	
}

public class MethodCallExample {
	public static void main(String[] args) {
		 Point p1 = new Point() ;
		 p1.x = 10 ;
		 p1.y = 5 ;
		 
		 Point p2 = new Point() ;
		 p2.x = 5 ;
		 p2.y = 2 ;
		 
		 System.out.println("p1プリントメソッドの呼び出し");
		 p1.printPotion();
		 
		 System.out.println("p2プリントメソッドの呼び出し");
		 p2.printPotion();
	}
}
