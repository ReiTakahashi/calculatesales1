package kensyu;



import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;


public class CalculateSales3 {
	public static void main(String[] args) {
		 System.out.println ("ここにあるファイルを開きます" + args[0]);
		 HashMap<String, String> shopkey = new HashMap<String, String>();


		BufferedReader br  = null ;
		try {
			File file = new File(args[0],"branch.lst" );
			FileReader fr = new FileReader(file) ;
			br = new BufferedReader(fr) ;

			String line  ;
			while((line = br.readLine()) !=null) {
				String[] shiten = line.split(",");

				shopkey.put(shiten[0], shiten[1]);
				System.out.println(shiten[0]) ;

			}

			} catch (IOException e) {
				System.out.println("エラーが発生しました。") ;
			} finally {
				if(br != null) {
					try {
						br.close();
					} catch (IOException e) {
						System.out.println("closeできませんでした。") ;
					}
				}
			}
		
	}
}
